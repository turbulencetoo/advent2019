use lazy_static::lazy_static;

use crate::intcode::{Intcode, parse_input};

static INPUT: &'static str = include_str!("input.txt");

lazy_static! {
    static ref INPUT_VEC: Vec<i64> = parse_input(INPUT);
}

pub fn run_a() {
    let mut computer = Intcode::new(INPUT_VEC.clone());
    computer.add_input(1);
    computer.run_day_5();
    let ans = computer.get_output();
    println!("Day 9a. The output of the boost program is {}", ans);
}

pub fn run_b() {
    let mut computer = Intcode::new(INPUT_VEC.clone());
    computer.add_input(2);
    computer.run_day_5();
    let ans = computer.get_output();
    println!("Day 9b. The output of the boost program is {}", ans);
}


#[test]
fn test_quine() {
    let input = vec![109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99];
    let mut computer = Intcode::new(input.clone());
    let output = computer.run_and_gather_all_output();
    dbg!(computer.get_prog());
    assert_eq!(input, output);
}

#[test]
fn test_large_output() {
    let input = vec![1102,34915192,34915192,7,4,7,99,0];
    let mut computer = Intcode::new(input.clone());
    let output = computer.run_and_gather_all_output();
    assert_eq!(output.len(), 1);
    assert_eq!(output[0].to_string().len(), 16);
}

#[test]
fn test_large_output2() {
    let input = vec![104,1125899906842624,99];
    let mut computer = Intcode::new(input.clone());
    let output = computer.run_and_gather_all_output();
    assert_eq!(output.len(), 1);
    assert_eq!(output[0], 1125899906842624);
}