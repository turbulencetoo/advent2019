mod intcode;

mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod day10;
mod day11;

fn main() {
    day1::run_a();
    day1::run_b();

    day2::run_a();
    day2::run_b();

    day3::run_a();
    day3::run_b();

    day4::run_a();
    day4::run_b();

    day5::run_a();
    day5::run_b();

    day6::run_a();
    day6::run_b();

    day7::run_a();
    day7::run_b();

    day8::run_a();
    day8::run_b();

    day9::run_a();
    day9::run_b();

    day10::run_a();
    day10::run_b();

    day11::run_a();
    day11::run_b();
}

