use lazy_static::lazy_static;

use crate::intcode::{Intcode, parse_input};

static INPUT: &'static str = include_str!("input.txt");

lazy_static! {
    static ref INPUT_VEC: Vec<i64> = parse_input(INPUT);
}

fn run_program(prog: Vec<i64>, input: i64) -> i64 {
    let mut computer = Intcode::new(prog);
    computer.add_input(input);
    assert!(computer.run_day_5());
    computer.get_output()
}

pub fn run_a() {
    let ans = run_program(INPUT_VEC.clone(), 1);
    println!("Day 5a. The output of the program with input 1 is {}", ans)
}

pub fn run_b() {
    let ans = run_program(INPUT_VEC.clone(), 5);
    println!("Day 5b. The output of the program with input 5 is {}", ans)
}
