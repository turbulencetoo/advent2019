use lazy_static::lazy_static;

use crate::intcode::{Intcode, parse_input};
use std::collections::HashMap;
use std::iter;

static INPUT: &'static str = include_str!("input.txt");

lazy_static! {
    static ref INPUT_VEC: Vec<i64> = parse_input(INPUT);
}

#[derive(Copy, Clone, Debug)]
enum Heading {
    N,
    S,
    E,
    W,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum Color {
    Black = 0,
    White = 1
}

impl From<i64> for Color{
    fn from(num: i64) -> Self {
        match num {
            0 => Color::Black,
            1 => Color::White,
            _ => panic!("bad color")
        }
    }
}

impl Heading {
    // 0 is left 1 is right
    fn turn(&self, turn: i64) -> Self {
        match (*self, turn) {
            (Heading::N, 0) => Heading::W,
            (Heading::N, 1) => Heading::E,
            (Heading::S, 0) => Heading::E,
            (Heading::S, 1) => Heading::W,
            (Heading::E, 0) => Heading::N,
            (Heading::E, 1) => Heading::S,
            (Heading::W, 0) => Heading::S,
            (Heading::W, 1) => Heading::N,
            (_, _) => unimplemented!()
        }
    }
    // return x,y
    fn to_dir(&self) -> (i64, i64) {
        match *self {
            Heading::N => (0, 1),
            Heading::S => (0, -1),
            Heading::E => (1, 0),
            Heading::W => (-1, 0),
        }
    }
}

struct Robot {
    pos: (i64, i64),
    heading: Heading,
    grid: HashMap<(i64, i64), Color>,
    computer: Intcode
}

impl Robot {
    fn new(input: Vec<i64>) -> Self {
        Self {
            pos: (0, 0),
            heading: Heading::N,
            grid: HashMap::new(),
            computer: Intcode::new(input)
        }
    }

    // paint then turn then move
    fn step(&mut self) -> bool {
        let (outputs, keep_running) = self.get_outputs();
        if let (Some(&color), Some(&direction)) = (outputs.get(0), outputs.get(1)) {
            self.grid.insert(self.pos, Color::from(color));
            self.heading = self.heading.turn(direction);
            let (delta_x, delta_y) = self.heading.to_dir();
            self.pos.0 += delta_x;
            self.pos.1 += delta_y;
        }
        keep_running
    }
    #[cfg(not(test))]
    fn get_outputs(&mut self) -> (Vec<i64>, bool) {
        // supply computer with input equal to where we are
        let iterative_input = Box::new(iter::repeat(
            *self.grid.get(&self.pos).unwrap_or(&Color::Black) as i64));
        self.computer.set_iterative_input(iterative_input);
        self.computer.run_and_gather_n_output(2)
    }
    #[cfg(test)]
    // there is no program, just test outputs
    fn get_outputs(&mut self) -> (Vec<i64>, bool) {
        thread_local! {
            static TEST_OUTPUTS: RefCell<Box<dyn Iterator<Item=(Vec<i64>, bool)>>>
            = RefCell::new(Box::new(vec![
                (vec![1,0], true),
                (vec![0,0], true),
                (vec![0,0], true),
                (vec![1,0], true),
                (vec![1,0], true),
                (vec![0,1], true),
                (vec![1,0], true),
                (vec![1,0], false),
            ].into_iter()));
        }
        TEST_OUTPUTS.with(|to| to.borrow_mut().next().unwrap())
    }
    fn set_origin_white(&mut self) {
        self.grid.insert((0, 0), Color::White);
    }
}

fn pretty_print(grid: &HashMap<(i64, i64), Color>) {
    const DIM: usize = 80;
    const ORIGIN: (usize, usize) = (DIM/2, DIM/2);
    let mut printable = [[' '; DIM]; DIM];
    for (coord, color) in grid.iter() {
        if *color == Color::White {
            // minus becasue y is inverted (bleh)
            let painting_coord_y = (ORIGIN.1 as i64 - coord.1) as usize;
            let painting_coord_x = (ORIGIN.0 as i64 + coord.0) as usize;
            printable[painting_coord_y][painting_coord_x] = '#';
        }
    }
    for row in printable.iter() {
        if row.iter().all(|&ch| ch == ' ') {
            continue
        }
        for ch in row.iter() {
            print!("{}", ch);
        }
        println!();
    }
}

pub fn run_a() {
    let mut robot = Robot::new(INPUT_VEC.clone());
    while robot.step() { }
    let ans = robot.grid.iter().count();
    println!("Day 11a. Number of squares painted is {}", ans);
}

pub fn run_b() {
    let mut robot = Robot::new(INPUT_VEC.clone());
    robot.set_origin_white();
    while robot.step() { }
    println!("Day 11b.");
    pretty_print(&robot.grid);
}

#[test]
fn test_robot() {
    let mut robot = Robot::new(INPUT_VEC.clone());
    while robot.step() { }
    let ans = robot.grid.iter().count();
    assert_eq!(ans, 6);
}