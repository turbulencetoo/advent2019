static INPUT: &'static str = include_str!("input.txt");

const WIDTH: usize = 25;
const HEIGHT: usize = 6;

fn parse_to_ints(input: &str) -> Vec<u8> {
    input
        .trim()
        .chars()
        .map(|ch| (ch as u64 - '0' as u64) as u8)
        .collect()
}

fn parse_to_layers(image: &[u8], layer_size: usize) -> Vec<Vec<u8>> {
    image.chunks(layer_size)
        .map(|chunk| chunk.iter().copied().collect())
        .collect()
}

// find layer with fewest zeros, count 1s and 2s in that layer and multiply the counts
pub fn run_a() {
    let layers = parse_to_layers(&parse_to_ints(INPUT), WIDTH * HEIGHT);
    let layer_with_fewest_zeros = layers
        .iter()
        .map(|layer| layer
            .iter()
            .filter(|&&digit| digit == 0)
            .count()
        )
        .enumerate()
        .min_by_key(|&(_, num_zeros)| num_zeros)
        .map(|(i ,_)| i)
        .unwrap();
    let num_ones = layers[layer_with_fewest_zeros]
        .iter()
        .filter(|&&digit| digit == 1)
        .count();
    let num_twos = layers[layer_with_fewest_zeros]
        .iter()
        .filter(|&&digit| digit == 2)
        .count();
    println!("Day 8a. The num(1) * num(2) in layer with min(num(0)) is {}", num_ones * num_twos);
}

// For each spot, take the pixel from the first layer that isn't 0
// convert to String of "0"s and "1"s
fn decode(input: &str, width: usize, height: usize ) -> String {
    let layers = parse_to_layers(&parse_to_ints(input), width * height);
    (0..(width*height))
        .map(|i| layers
            .iter()
            .filter(|layer| layer[i] != 2)
            .next()
            .map(|layer| layer[i])
            .unwrap()
        )
        .map(|num| num.to_string())
        .collect::<Vec<_>>()
        .join("")
}

// Pretty print the output image of 0s and 1s as '■' and ' '
fn print_image(image: &str) {
    const BOX: char = '■';
    let printable = image
        .chars()
        .map(|ch| match ch {
            '0' => ' ',
            '1' => BOX,
            _ => unreachable!()
        })
        .collect::<String>();
    for (i, ch) in printable.chars().enumerate() {
        if (i != 0) && (i % WIDTH == 0) {
            println!()
        }
        print!("{}", ch)
    }
    println!();
}

pub fn run_b() {
    let ans = decode(INPUT, WIDTH, HEIGHT);
    println!("Day 8b. The final image is");
    print_image(&ans);
}

#[test]
fn test_decode() {
    assert_eq!(decode("0222112222120000", 2, 2), "0110");
}

