use lazy_static::lazy_static;
use num::Integer;
use std::collections::HashMap;

static INPUT: &'static str = include_str!("input.txt");

fn parse_input(input: &str) -> Vec<Vec<bool>> {
    input.lines()
        .map(|line|
            line.trim()
                .chars()
                .map(|ch| ch == '#')
                .collect::<Vec<_>>()
        )
        .collect()
}

lazy_static! {
    static ref INPUT_GRID: Vec<Vec<bool>> = parse_input(INPUT);
}

// returns y, x from upper left
fn best_loc(grid: &Vec<Vec<bool>>) -> ((usize, usize), u32) {
    let mut best = None;
    let mut best_number = 0u32;
    for y in 0..grid.len() {
        for x in 0..grid[y].len() {
            if grid[y][x] {
                let num_seen = count_num_seen(grid, (y,x));
                if num_seen > best_number {
                    best.replace(((y,x), num_seen));
                    best_number = num_seen;
                }
            }
        }
    }
    best.unwrap()
}


fn map_other_asteroids_to_directions(grid: &Vec<Vec<bool>>, origin: (usize, usize))
-> HashMap<(isize, isize), Vec<(usize, usize)>> {
    let mut out = HashMap::new();
    for y in 0..grid.len() {
        for x in 0..grid[y].len() {
            if (y, x) == origin {
                continue;
            }
            if !grid[y][x] {
                continue;
            }
            let mut dir = (y as isize - origin.0 as isize,
                           x as isize - origin.1 as isize);
            // reduce dir to smallest step size
            let gcd = dir.0.gcd(&dir.1);
            dir = (dir.0 / gcd, dir.1 / gcd);
            out.entry(dir).or_insert_with(|| vec![]).push((y,x));
        }
    }
    out
}

fn count_num_seen(grid: &Vec<Vec<bool>>, origin: (usize, usize)) -> u32 {
    let map = map_other_asteroids_to_directions(grid, origin);
    map.iter().count() as u32
}

// (y2 - y1)^2 + (x2-x1)^2
fn distance_between(asteroid: (usize, usize), origin: (usize, usize)) -> usize {
    ((asteroid.0 as isize - origin.0 as isize).pow(2)
        + (asteroid.1 as isize - origin.1 as isize).pow(2)
    ) as usize
}

// Turn the direction -> asteroids map into a sorted list where the first arrays are closer
// to 12 oclock
// also sort the internal arrays so closest asteriods to origin can be popped off the end
fn convert_dir_map_to_sorted_lists(map: HashMap<(isize, isize), Vec<(usize, usize)>>,
                                   origin: (usize, usize)) -> Vec<Vec<(usize, usize)>> {
    let mut intermediate: Vec<((isize, isize), Vec<(usize, usize)>)> =
        map.into_iter().collect();
    intermediate.sort_by(|a, b|
        angle_relative_to_12_oclock(a.0).partial_cmp(
            &angle_relative_to_12_oclock(b.0)).unwrap()
    );

    for key_val in intermediate.iter_mut() {
        key_val.1.sort_by_key(|&asteroid| distance_between(asteroid, origin));
        // closest asteroid need to be at end of list
        key_val.1.reverse();
    }
    intermediate.into_iter().map(|key_val| key_val.1).collect()
}


// tan(θ) = x/y in quadrants I and III, θ = arctan(x/y)
// in II and IV, θ = arctan(y/x)
//  II  |  I
//  --------
//  III | IV
fn angle_relative_to_12_oclock(dir: (isize, isize)) -> f64 {
    // y = -1, x=0 is 12 oclock and we want that to be 0 radians
    // first make both positive to we get an acute angle < pi/2
    let yabs = dir.0.abs() as f64;
    let xabs = dir.1.abs() as f64;
    if dir.0 <= 0 && dir.1 >= 0 {
        // y is negavtive, x is positive
        //quadrant I is correct
        (xabs/yabs).atan()
    }
    else if dir.0 > 0 && dir.1 > 0 {
        // quadrant IV
        (yabs/xabs).atan() + std::f64::consts::FRAC_PI_2
    }
    else if dir.0 >= 0 && dir.1 <= 0 {
        // quadrant III
        (xabs/yabs).atan() + 2f64 * std::f64::consts::FRAC_PI_2
    }
    else if dir.0 < 0 && dir.1 < 0 {
        // quadrant II
        (yabs/xabs).atan() + 3f64 * std::f64::consts::FRAC_PI_2
    }
    else {
        panic!("All quadrants should be covered!");
    }
}

fn vaporized_order(grid: &Vec<Vec<bool>>, origin: (usize, usize)) -> Vec<(usize, usize)> {
    let map = map_other_asteroids_to_directions(grid, origin);
    let mut ordered = convert_dir_map_to_sorted_lists(map, origin);
    let mut out: Vec<(usize, usize)> = vec![];

    loop {
        ordered.retain(|list| !list.is_empty());
        if ordered.is_empty() {
            return out;
        }
        for list in ordered.iter_mut() {
            let closest_asteroid = list.pop().unwrap();
            out.push(closest_asteroid);
        }

    }
}

pub fn run_a() {
    let ans = best_loc(&INPUT_GRID);
    println!("Day 10a. Best location (y,x) is {:?}, with {} asteroids seen",
             ans.0, ans.1);
}


pub fn run_b() {
    let ans = vaporized_order(&INPUT_GRID, (17,13))[199];
    println!("Day 10b. 200th vaporized asteroid (y,x) is {:?}", ans);
}

#[test]
fn test_best_loc() {
    // Remember that I use y,x and they use x,y
    let mut input =
r".#..#
.....
#####
....#
...##";
    let mut parsed = parse_input(input);
    assert_eq!(best_loc(&parsed), ((4,3), 8));

    input =
r"......#.#.
#..#.#....
    ..#######.
.#.#.###..
    .#..#.....
    ..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####";
    parsed = parse_input(input);
    assert_eq!(best_loc(&parsed), ((8,5), 33));

    input =
r".#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##";
    parsed = parse_input(input);
    assert_eq!(best_loc(&parsed), ((13,11), 210));
}

#[test]
fn test_angle() {
    let coords = vec![(-1,0), (-1,1), (0,1), (1,2), (1,0), (1, -3), (0,-1), (-1, -4)];
    let rads = coords.into_iter().map(angle_relative_to_12_oclock).collect::<Vec<_>>();
    println!("{:?}", rads);
    for pair in rads.windows(2) {
        assert!(pair[0] < pair[1]);
    }
}

#[test]
fn test_vaporize_order(){
    let mut input =
r".#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.........###..
..#.#.....#....##";
    let mut parsed = parse_input(input);
    let mut order = vaporized_order(&parsed, (3,8));
    assert_eq!(&order[0..2], &[(1,8), (0,9)]);
    assert_eq!(*order.last().unwrap(), (3,14));


    input =
r".#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##";
    parsed = parse_input(input);
    order = vaporized_order(&parsed, (13,11));
    assert_eq!(order[0], (12,11));
    assert_eq!(order[1], (1,12));
    assert_eq!(order[2], (2,12));
    assert_eq!(order[9], (8,12));
    assert_eq!(order[19], (0,16));
    assert_eq!(order[49], (9,16));
    assert_eq!(order[99], (16,10));
    assert_eq!(order[198], (6,9));
    assert_eq!(order[199], (2,8));
    assert_eq!(order[200], (9,10));
    assert_eq!(order[298], (1,11));
}