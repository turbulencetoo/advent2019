use std::collections::VecDeque;

enum Status {
    Continue(usize),
    Done,
    Output(usize) // Like continue, but indicates that an output was just written
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
enum VarMode {
    Position = 0,
    Immediate = 1,
    Relative = 2,
}

#[derive(Debug)]
enum OpCode {
    Add = 1,
    Mul = 2,
    Input = 3,
    Output = 4,
    JumpIfTrue = 5,
    JumpIfFalse = 6,
    LessThan = 7,
    Equals = 8,
    SetRelBase = 9,
    Halt = 99
}

#[derive(Debug)]
pub enum SevenB {
    Output,
    Halt
}

pub struct Intcode {
    ip: usize,
    prog: Vec<i64>,
    input_queue: VecDeque<i64>,
    output: i64,
    relative_base: isize,
    // For day 11 where we need to collect input N times. probably will need refactor
    input_iterator: Option<Box<dyn Iterator<Item=i64>>>
}

impl Intcode {

    pub fn new(prog: Vec<i64>) -> Self {
        Self {
            ip: 0usize,
            prog,
            input_queue: VecDeque::new(),
            output: 0,
            relative_base: 0,
            input_iterator: None
        }
    }

    pub fn add_input(&mut self, input: i64) {
        self.input_queue.push_back(input);
    }

    pub fn set_iterative_input(&mut self, input_iterator: Box<dyn Iterator<Item=i64>>) {
        self.input_iterator = Some(input_iterator);
    }

    pub fn get_output(&self) -> i64 {
        self.output
    }

    // functions to expand memory if it doesn't exist
    // for now just expand the vec but if indices can be arbitrarily
    // large and sparse then maybe use a map
    fn ensure_in_bounds(&mut self, addr: usize) {
        if addr >= self.prog.len() {
            let needed = (addr - self.prog.len()) + 1;
            self.prog.reserve(needed);
            for _ in 0..needed {
                self.prog.push(0);
            }
        }
    }

    fn read(&mut self, addr: usize) -> i64 {
        self.ensure_in_bounds(addr);
        self.prog[addr]
    }

    fn write(&mut self, addr: usize, val: i64) {
        self.ensure_in_bounds(addr);
        self.prog[addr] = val;
    }

    // before running the program for day 2, replace positions 1 and 2
    pub fn set_noun_verb(&mut self, noun: i64, verb: i64) {
        self.prog[1] = noun;
        self.prog[2] = verb;
    }

    fn parse_opcode(&mut self) -> (OpCode, Vec<VarMode>) {
        let to_be_parsed = self.read(self.ip);
        let opcode = match to_be_parsed % 100 {
            1 => OpCode::Add,
            2 => OpCode::Mul,
            3 => OpCode::Input,
            4 => OpCode::Output,
            5 => OpCode::JumpIfTrue,
            6 => OpCode::JumpIfFalse,
            7 => OpCode::LessThan,
            8 => OpCode::Equals,
            9 => OpCode::SetRelBase,
            99 => OpCode::Halt,
            other => panic!("Bad opcode: {}. Parsed from number: {}",
                          other, to_be_parsed)
        } ;
        let mut remaining = to_be_parsed / 100;
        let mut modes = Vec::new();
        while remaining > 0 {
            modes.push(
                match remaining % 10 {
                    0 => VarMode::Position,
                    1 => VarMode::Immediate,
                    2 => VarMode::Relative,
                    _ => unimplemented!()
                }
            );
            remaining /= 10;
        }
        (opcode, modes)
    }

    fn get_idx(&mut self, idx: usize, mode: VarMode) -> usize {
        match mode {
            VarMode::Position => self.read(idx) as usize,
            VarMode::Immediate => idx,
            VarMode::Relative => (self.read(idx) as isize + self.relative_base) as usize,
        }
    }

    fn get_val(&mut self, idx: usize, mode: VarMode) -> i64 {
        let true_idx = self.get_idx(idx, mode);
        self.read(true_idx)
    }

    fn add(&mut self, modes: Vec<VarMode>) -> Status {
        const ARITY: usize = 3;

        let val_a = self.get_val(
            self.ip + 1,
            *modes.get(0).unwrap_or(&VarMode::Position)
        );
        let val_b = self.get_val(
            self.ip + 2,
            *modes.get(1).unwrap_or(&VarMode::Position)
        );
        let idx_dest = self.get_idx(
            self.ip + 3,
            *modes.get(2).unwrap_or(&VarMode::Position)
        );

        self.write(idx_dest,  val_a + val_b);
        Status::Continue(ARITY + 1)
    }

    fn mul(&mut self, modes: Vec<VarMode>) -> Status {
        const ARITY: usize = 3;

        let val_a = self.get_val(
            self.ip + 1,
            *modes.get(0).unwrap_or(&VarMode::Position)
        );
        let val_b = self.get_val(
            self.ip + 2,
            *modes.get(1).unwrap_or(&VarMode::Position)
        );
        let idx_dest = self.get_idx(
            self.ip + 3,
            *modes.get(2).unwrap_or(&VarMode::Position)
        );

        self.write(idx_dest, val_a * val_b);

        Status::Continue(ARITY + 1)
    }

    fn input(&mut self, modes: Vec<VarMode>) -> Status {
        const ARITY: usize = 1;

        // "Parameters that an instruction writes to will never be in immediate mode."
        assert_ne!(Some(&VarMode::Immediate), modes.get(0));

        let idx_dest = self.get_idx(
            self.ip + 1,
            *modes.get(0).unwrap_or(&VarMode::Position)
        );

        let input_val =
            if let Some(ref mut it) = self.input_iterator {
                it.next().unwrap()
            }
            else {
                self.input_queue.pop_front().unwrap()
            };
        self.write(idx_dest, input_val);

        Status::Continue(ARITY + 1)
    }

    fn output(&mut self, modes: Vec<VarMode>) -> Status {
        const ARITY: usize = 1;

        let idx_src = self.get_idx(
            self.ip + 1,
            *modes.get(0).unwrap_or(&VarMode::Position)
        );

        self.output = self.read(idx_src);

        Status::Output(ARITY +1)
    }

    fn jump_if_true(&mut self, modes: Vec<VarMode>) -> Status {
        const ARITY: usize = 2;

        let val_a = self.get_val(
            self.ip + 1,
            *modes.get(0).unwrap_or(&VarMode::Position)
        );
        let val_b = self.get_val(
            self.ip + 2,
            *modes.get(1).unwrap_or(&VarMode::Position)
        );
        if val_a != 0 {
            self.ip = val_b as usize;
            Status::Continue(0)
        }
        else{
            Status::Continue(ARITY + 1)
        }
    }

    fn jump_if_false(&mut self, modes: Vec<VarMode>) -> Status {
        const ARITY: usize = 2;

        let val_a = self.get_val(
            self.ip + 1,
            *modes.get(0).unwrap_or(&VarMode::Position)
        );
        let val_b = self.get_val(
            self.ip + 2,
            *modes.get(1).unwrap_or(&VarMode::Position)
        );
        if val_a == 0 {
            self.ip = val_b as usize;
            Status::Continue(0)
        }
        else {
            Status::Continue(ARITY + 1)
        }
    }

    fn less_than(&mut self, modes: Vec<VarMode>) -> Status {
        const ARITY: usize = 3;

        let val_a = self.get_val(
            self.ip + 1,
            *modes.get(0).unwrap_or(&VarMode::Position)
        );
        let val_b = self.get_val(
            self.ip + 2,
            *modes.get(1).unwrap_or(&VarMode::Position)
        );
        let idx_dest = self.get_idx(
            self.ip + 3,
            *modes.get(2).unwrap_or(&VarMode::Position)
        );
        self.write(idx_dest, (val_a < val_b) as i64);

        Status::Continue(ARITY + 1)
    }
    fn equals(&mut self, modes: Vec<VarMode>) -> Status {
        const ARITY: usize = 3;

        let val_a = self.get_val(
            self.ip + 1,
            *modes.get(0).unwrap_or(&VarMode::Position)
        );
        let val_b = self.get_val(
            self.ip + 2,
            *modes.get(1).unwrap_or(&VarMode::Position)
        );
        let idx_dest = self.get_idx(
            self.ip + 3,
            *modes.get(2).unwrap_or(&VarMode::Position)
        );
        self.write(idx_dest, (val_a == val_b) as i64);

        Status::Continue(ARITY + 1)
    }

    fn set_rel_base(&mut self, modes: Vec<VarMode>) -> Status {
        const ARITY: usize = 1;

        let base_adjustment = self.get_val(
            self.ip + 1,
            *modes.get(0).unwrap_or(&VarMode::Position)
        );
        self.relative_base += base_adjustment as isize;

        Status::Continue(ARITY + 1)
    }

    fn step_once(&mut self) -> Status {
        let (opcode, modes) = self.parse_opcode();
        match opcode {
            OpCode::Add => self.add(modes),
            OpCode::Mul => self.mul(modes),
            OpCode::Input => self.input(modes),
            OpCode::Output => self.output(modes),
            OpCode::JumpIfTrue => self.jump_if_true(modes),
            OpCode::JumpIfFalse => self.jump_if_false(modes),
            OpCode::LessThan => self.less_than(modes),
            OpCode::Equals => self.equals(modes),
            OpCode::SetRelBase => self.set_rel_base(modes),
            OpCode::Halt => Status::Done
        }
    }


    // run program to completion, consuming self
    // return the finished program, or None if an instruction caused out of bounds
    pub fn run_day_2(&mut self) -> Option<Vec<i64>> {
        loop {
            match self.step_once() {
                Status::Continue(step_size) => self.ip += step_size,
                Status::Done => return Some(self.get_prog()),
                Status::Output(_) => unimplemented!() // Output not an opcode for day 2
            }
        }
    }

    // return whether finished successfully
    pub fn run_day_5(&mut self) -> bool {
        loop {
            match self.step_once() {
                Status::Continue(step_size) => self.ip += step_size,
                Status::Done => return true,
                Status::Output(step_size) => self.ip += step_size
            }
        }
    }

    pub fn run_day_7b(&mut self) -> SevenB {
        loop {
            match self.step_once() {
                Status::Continue(step_size) => self.ip += step_size,
                Status::Done => return SevenB::Halt,
                Status::Output(step_size) => {
                    self.ip += step_size;
                    return SevenB::Output
                },
            }
        }
    }
    pub fn get_prog(&self) -> Vec<i64> {
        self.prog.clone()
    }


    pub fn run_and_gather_n_output(&mut self, n: usize) -> (Vec<i64>, bool) {
        let mut out = vec![];
        loop {
            match self.step_once() {
                Status::Continue(step_size) => self.ip += step_size,
                Status::Done => return (out, false),
                Status::Output(step_size) => {
                    self.ip += step_size;
                    out.push(self.get_output());
                    if out.len() == n {
                        return (out, true);
                    }
                },
            }
        }
    }

    #[cfg(test)]
    pub fn run_and_gather_all_output(&mut self) -> Vec<i64> {
        let mut out = vec![];
        loop {
            match self.step_once() {
                Status::Continue(step_size) => self.ip += step_size,
                Status::Done => return out,
                Status::Output(step_size) => {
                    self.ip += step_size;
                    out.push(self.get_output());
                },
            }
        }
    }
}

pub fn parse_input(input: &str) -> Vec<i64> {
    input
        .trim()
        .split(',')
        .map(|num| num.parse::<i64>().unwrap())
        .collect()
}