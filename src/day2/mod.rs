use lazy_static::lazy_static;

use crate::intcode::{Intcode, parse_input};

static INPUT: &'static str = include_str!("input.txt");

lazy_static! {
    static ref INPUT_VEC: Vec<i64> = parse_input(INPUT);
}

fn run_program(noun: i64, verb: i64) -> Option<Vec<i64>> {
    let mut computer = Intcode::new(INPUT_VEC.clone());
    computer.set_noun_verb(noun, verb);
    computer.run_day_2()
}

pub fn run_a() {
    let ans = run_program(12,2);
    println!("Day 2a. After program halts, position 0 holds {}", ans.unwrap()[0]);
}

// try a bunch of numbers
pub fn run_b() {
    let ans = (0i64..100)
        .flat_map(|i| (0i64..100)
            .map(move |j| (run_program(i, j), i, j))
        )
        .filter(|(out, _, _)| out.is_some())
        .filter(|(out, _, _)| out.as_ref().unwrap()[0] == 19690720)
        .map(|(_, noun, verb)| 100 * noun + verb)
        .next()
        .unwrap();

    println!("Day 2b. After many tries, 100 * noun + verb = {}", ans);
}

#[cfg(test)]
fn test_program(prog: Vec<i64>) -> Option<Vec<i64>> {
    let mut computer = Intcode::new(prog);
    computer.run_day_2()
}
#[test]
fn test_run_program() {
    assert_eq!(test_program(vec![1,0,0,0,99]), Some(vec![2,0,0,0,99]));
    assert_eq!(test_program(vec![2,3,0,3,99]), Some(vec![2,3,0,6,99]));
    assert_eq!(test_program(vec![2,4,4,5,99,0]), Some(vec![2,4,4,5,99,9801]));
    assert_eq!(test_program(vec![1,1,1,4,99,5,6,0,99]), Some(vec![30,1,1,4,2,5,6,0,99]));
}