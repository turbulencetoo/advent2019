use std::ops::Range;

static INPUT: &'static str = include_str!("input.txt");
const NUM_DIGITS: usize = 6;

fn parse_input() -> Range<u32> {
    let mut nums = INPUT
        .trim()
        .split('-')
        .map(|num| num.parse::<u32>().unwrap());
    let start = nums.next().unwrap();
    let end = nums.next().unwrap();
    start..end
}

// turn a number into a list of decimal digits
// least significant first!
fn convert_to_digits(mut num: u32) -> [u8; NUM_DIGITS] {
    let mut out = [0u8; NUM_DIGITS];
    let mut i = 0usize;
    while num > 0 {
        out[i] = (num % 10) as u8;
        num /= 10;
        i += 1;
    }
    out
}

// digits are in backwards order (least significant first)
// so must not INCREASE and must have one double
fn is_sequential_with_double(digits: &[u8]) -> bool {
    let mut double_seen = false;
    for pair in digits.windows(2) {
        if pair[0] < pair[1] {
            return false
        }
        if pair[0] == pair[1] {
            double_seen = true;
        }
    }
    double_seen
}

// return whether there's a double that is not part of a triple or larger
fn contains_true_double(digits: &[u8]) -> bool {
    for i in 0..(digits.len() - 1) {
        if digits[i] == digits[i+1] {
            if (i == 0 || digits[i-1] != digits[i])
                && (i == digits.len() - 2 || digits[i+2] != digits[i]) {
                return true;
            }
        }
    }
    false
}

fn is_password_part_a(pass: u32) -> bool {
    is_sequential_with_double(&convert_to_digits(pass))
}

fn is_password_part_b(pass: u32) -> bool {
    let digits = convert_to_digits(pass);
    is_sequential_with_double(&digits) && contains_true_double(&digits)
}


pub fn run_a() {
    let num_valid_passwords = parse_input()
        .filter(|&num| is_password_part_a(num))
        .count();
    println!("Day 4a. The number of valid passwords in the range is {}",
             num_valid_passwords);
}

pub fn run_b() {
    let num_valid_passwords = parse_input()
        .filter(|&num| is_password_part_b(num))
        .count();
    println!("Day 4b. The number of valid passwords in the range is {}",
             num_valid_passwords);
}

#[test]
fn test_is_password() {
    assert!(is_password_part_a(111111));
    assert!(!is_password_part_a(223450));
    assert!(!is_password_part_a(123789));
}

#[test]
fn test_is_password_part_b() {
    assert!(is_password_part_b(112233));
    assert!(!is_password_part_b(123444));
    assert!(is_password_part_b(111122));
}