use permutohedron::Heap;
use lazy_static::lazy_static;

use crate::intcode::{Intcode, parse_input, SevenB};

static INPUT: &'static str = include_str!("input.txt");
lazy_static! {
    static ref INPUT_VEC: Vec<i64> = parse_input(INPUT);
}

const NUM_PROGS: usize = 5;

struct FeedbackLoop {
    cur: usize,
    pipe: i64,  // Output to be fed to next input
    amplifiers: Vec<Intcode>
}

impl FeedbackLoop {
    fn new(prog: &Vec<i64>, inputs: &[i64]) -> Self {
        let mut amplifiers = (0..NUM_PROGS)
            .map(|_| Intcode::new(prog.clone()))
            .collect::<Vec<_>>();
        for (i, amp) in amplifiers.iter_mut().enumerate() {
            amp.add_input(inputs[i]);
        }
        Self{
            cur: 0,
            pipe: 0, // To start the process, a 0 signal is sent to amplifier A's input exactly once.
            amplifiers
        }
    }
    fn run(&mut self) -> i64 {
        loop {
            let computer = &mut self.amplifiers[self.cur];
            computer.add_input(self.pipe);
            match computer.run_day_7b() {
                SevenB::Output => {
                    self.pipe = computer.get_output();
                    self.cur += 1;
                    self.cur %= NUM_PROGS;
                }
                SevenB::Halt => {
                    if self.cur == NUM_PROGS-1 {
                        return computer.get_output();
                    }
                    else {
                        self.pipe = computer.get_output();
                        self.cur += 1;
                        self.cur %= NUM_PROGS;
                    }
                }
            }
        }
    }
}

const FIRST_INPUT: i64 = 0;

fn run_program(prog: Vec<i64>, input_seed: i64, input_signal: i64) -> i64 {
    let mut computer = Intcode::new(prog);
    computer.add_input(input_seed);
    computer.add_input(input_signal);
    assert!(computer.run_day_5());
    computer.get_output()
}

fn try_set_of_inputs(inputs: &[i64], prog: &Vec<i64>) -> i64 {
    let mut cur = run_program(prog.clone(), inputs[0], FIRST_INPUT);
    for i in 1..inputs.len() {
        cur = run_program(prog.clone(), inputs[i], cur);
    }
    cur
}

fn max_thruster_signal(prog: &Vec<i64>) -> i64 {
    let mut data = vec![0, 1, 2, 3, 4];
    let heap = Heap::new(&mut data);

    heap.into_iter()
        .map(|permutation| try_set_of_inputs(&permutation, prog))
        .max()
        .unwrap()
}

fn try_inputs_for_feedback(inputs: &[i64], prog: &Vec<i64>) -> i64 {
    let mut feedback = FeedbackLoop::new(prog, inputs);
    feedback.run()
}

fn max_thruster_feedback(prog: &Vec<i64>) -> i64 {
    let mut data = vec![5, 6, 7, 8, 9];
    let heap = Heap::new(&mut data);

    heap.into_iter()
        .map(|permutation| try_inputs_for_feedback(&permutation, prog))
        .max()
        .unwrap()
}

pub fn run_a() {
    let ans = max_thruster_signal(&INPUT_VEC);
    println!("Day 7a. Max thruster signal is {}", ans);
}

pub fn run_b() {
    let ans = max_thruster_feedback(&INPUT_VEC);
    println!("Day 7b. Max thruster signal is {}", ans);
}

#[test]
fn test_max_thruster_signal() {
    assert_eq!(
        max_thruster_signal(
            &vec![3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0]
        ),
        43210
    );
    assert_eq!(
        max_thruster_signal(
            &vec![3,23,3,24,1002,24,10,24,1002,23,-1,23,
                 101,5,23,23,1,24,23,23,4,23,99,0,0]
        ),
        54321
    );
    assert_eq!(
        max_thruster_signal(
            &vec![3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
                 1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0]
        ),
        65210
    );
}

#[test]
fn test_max_thruster_feedback() {
    assert_eq!(
        max_thruster_feedback(
            &vec![3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,
                  27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5]
        ),
        139629729
    );
    assert_eq!(
        max_thruster_feedback(
            &vec![3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,
                  -5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,
                  53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10]
        ),
        18216
    );
}