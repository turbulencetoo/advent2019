use itertools::Itertools;

use std::collections::HashMap;

static INPUT: &'static str = include_str!("input.txt");

// A child directly orbits its parent
// store only string indices instead of references
struct Planet {
    parent: Option<String>,
    children: Vec<String>
}

impl Planet {
    fn new() -> Self {
        Planet {
            parent: None,
            children: vec![]
        }
    }
}

struct OrbitGraph {
    // Hold all the objects by their key in one place
    master: HashMap<String, Planet>
}

impl OrbitGraph {
    fn from_input(input: &str) -> Self {
        let mut out = Self { master: HashMap::new() };
        input
            .lines()
            .flat_map(|line| line
                .trim()
                .split(')')
                .tuples::<(_,_)>())
                .for_each(|(p, c)| out.add_parent_child(p, c));
        out
    }

    // helper fn to add a parent-child relationship to the graph
    // adds either parent or child planet if not existing
    fn add_parent_child(&mut self,
                        parent_key: &str,
                        child_key: &str) {
        let parent = self.master.entry(parent_key.to_owned())
            .or_insert_with(||Planet::new());
        parent.children.push(child_key.to_owned());

        let child =  self.master.entry(child_key.to_owned())
            .or_insert_with(||Planet::new());
        child.parent.replace(parent_key.to_owned());
    }

    fn get_root(&self) -> &Planet {
        self.master
            .values()
            .filter(|planet| planet.parent.is_none())
            .next()
            .unwrap()
    }

    fn count_orbits_recursive(&self, cur: &Planet, level: u32) -> u32 {
        level + cur.children
            .iter()
            .map(|child_key| &self.master[child_key])
            .map(|child| self.count_orbits_recursive(child, level+1))
            .sum::<u32>()
    }

    fn count_orbits(&self) -> u32 {
        self.count_orbits_recursive(self.get_root(), 0)
    }

    // Make a list of how to get from COM down to planet
    // start at planet and work up, then reverse it
    fn generate_path_to_planet<'a>(&'a self, idx: &'a str) -> Vec<&'a str> {
        let mut out = vec![];
        let mut planet = &self.master[idx];
        out.push(idx);

        while let Some(ref parent_idx) = planet.parent{
            out.push(parent_idx);
            planet = &self.master[parent_idx];
        }
        out.reverse();
        out
    }
}

// Given the paths to each object, find closest common ancestor
// Then see how far down you have to go from ancestor to reach each object
// Ex. if common ancestor is at depth 4, you are at depth 6 and santa is at depth 9
// path is (6-4) + (9-4) = 7 long.
fn find_dist(path_a: &[&str], path_b: &[&str]) -> u32 {
    let mut common_ancestor = 0usize;

    // stop looping when the _next_ item doesn't match, so common_ancestor var
    // holds the last index where there was a match
    while path_a[common_ancestor+1] == path_b[common_ancestor+1] {
        common_ancestor += 1;
    }

    // To find an object's orbital depth subtract 1 from it's path's length
    // because len is one longer than the path array.
    // subtract another 1 because last item in path array is the object, and we're
    // only concerned with what that object is orbiting
    let a_depth = path_a.len() - 2;
    let b_depth = path_b.len() - 2;

    // then subtract out the common ancestor to get relative depth
    ((a_depth - common_ancestor) + (b_depth - common_ancestor)) as u32
}


pub fn run_a() {
    let graph = OrbitGraph::from_input(INPUT);
    println!("Day 6a. Number of direct and indirect orbits is {}",
             graph.count_orbits());
}

pub fn run_b() {
    let graph = OrbitGraph::from_input(INPUT);
    let my_path = graph.generate_path_to_planet("YOU");
    let santa_path = graph.generate_path_to_planet("SAN");

    println!("Day 6b. Distance between me and santa is {}",
             find_dist(&my_path, &santa_path));
}


#[test]
fn test_orbit_counter() {
    let input = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L";
    let graph = OrbitGraph::from_input(input);
    assert_eq!(graph.count_orbits(), 42);
}

#[test]
fn test_path_finder() {
    let input = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN";
    let graph = OrbitGraph::from_input(input);
    let my_path = graph.generate_path_to_planet("YOU");
    let santa_path = graph.generate_path_to_planet("SAN");
    assert_eq!(find_dist(&my_path, &santa_path), 4);
}
