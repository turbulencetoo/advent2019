
// Turn newline-separated numbers into vec of masses
fn parse_input() -> Vec<u32> {
    include_str!("input.txt")
        .lines()
        .map(|line| line.trim().parse::<u32>().unwrap())
        .collect()
}

// "take its mass, divide by three, round down, and subtract 2"
fn calculate_req_fuel(mass: u32) -> u32 {
    let intermediate = (mass as f64 / 3.0f64).floor() as u32;
    if intermediate > 2u32 {
        intermediate - 2u32
    }
    else {
        0u32
    }
}

// calculate fuel needed for module and its fuel, and its fuel's fuel etc
fn calculate_req_fuel_recursive(mass: u32) -> u32 {
    let mut sum = 0u32;
    let mut cur = mass;
    while cur > 0 {
        cur = calculate_req_fuel(cur);
        sum += cur;
    }
    sum
}


pub fn run_a() {
    let total_req_fuel: u32 = parse_input()
        .into_iter()
        .map(calculate_req_fuel)
        .sum();
    println!("Day 1a. Fuel required for all spacecraft modules is {}",
             total_req_fuel);
}

pub fn run_b() {
    let total_req_fuel: u32 = parse_input()
        .into_iter()
        .map(calculate_req_fuel_recursive)
        .sum();
    println!("Day 1b. Fuel required for all spacecraft modules plus fuel is {}",
             total_req_fuel);
}


#[test]
// From test cases given
fn test_req_fuel() {
    assert_eq!(calculate_req_fuel(12), 2);
    assert_eq!(calculate_req_fuel(14), 2);
    assert_eq!(calculate_req_fuel(1969), 654);
    assert_eq!(calculate_req_fuel(100756), 33583);
}

#[test]
// From test cases given
fn test_req_fuel_recursive() {
    assert_eq!(calculate_req_fuel_recursive(14), 2);
    assert_eq!(calculate_req_fuel_recursive(1969), 966);
    assert_eq!(calculate_req_fuel_recursive(100756), 50346);
}