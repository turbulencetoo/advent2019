use std::collections::HashMap;

static INPUT: &'static str = include_str!("input.txt");

#[derive(Copy, Clone)]
struct Instruction {
    dir: (i32, i32),
    num: i32
}

fn parse_input(input: &str) -> [Vec<Instruction>; 2] {
    let mut lines = input.lines();
    [
        parse_instructions(lines.next().unwrap()),
        parse_instructions(lines.next().unwrap())
    ]
}

fn parse_instructions(line: &str) -> Vec<Instruction> {
    let mut out = vec![];

    for instruction in line.trim().split(',') {
        let dir = match instruction.chars().nth(0).unwrap() {
            'L' => (-1, 0),
            'R' => (1, 0),
            'U' => (0, 1),
            'D' => (0, -1),
            _ => unimplemented!()
        };
        let num = instruction[1..].parse::<i32>().unwrap();
        out.push(Instruction{ dir, num })
    }
     out
}

// Take a vec of instructions and generate a map of coordinates that are used
// Maps to when it was first seen
// coords are x,y
fn instructions_to_visited_coords(instructions: &[Instruction]) -> HashMap<(i32, i32), i32> {
    let mut cur = (0i32, 0i32);
    let mut out = HashMap::new();
    let mut num_steps = 1;
    for instruction in instructions {
        let num = instruction.num;
        let dir = instruction.dir;
        for mult in 1..=num {
            let new = (cur.0 + mult * dir.0, cur.1 + mult * dir.1);
            if !out.contains_key(&new) {
                out.insert(new, num_steps);
            }
            num_steps += 1
        }
        cur = (cur.0 + num * dir.0, cur.1 + num * dir.1)
    }
    out
}

fn man_dist(point: (i32, i32)) -> i32 {
    point.0.abs() + point.1.abs()
}

fn closest_interesction(a: HashMap<(i32, i32), i32>,
                        b: HashMap<(i32, i32), i32>) -> i32 {
    a.keys()
        .filter(|key| b.contains_key(key))
        .map(|&point| man_dist(point))
        .min()
        .unwrap()
}

fn find_closest_from_input(input: &str) -> i32 {
    let instruction_sets = parse_input(input);
    let set_a = instructions_to_visited_coords(&instruction_sets[0]);
    let set_b = instructions_to_visited_coords(&instruction_sets[1]);
    closest_interesction(set_a, set_b)
}

fn short_circuit_intersection(a: HashMap<(i32, i32), i32>,
                              b: HashMap<(i32, i32), i32>) -> i32 {
    a.keys()
        .filter(|key| b.contains_key(key))
        .map(|point| a[point] + b[point])
        .min()
        .unwrap()
}

fn find_shortest_circuit_from_input(input: &str) -> i32 {
    let instruction_sets = parse_input(input);
    let set_a = instructions_to_visited_coords(&instruction_sets[0]);
    let set_b = instructions_to_visited_coords(&instruction_sets[1]);
    short_circuit_intersection(set_a, set_b)
}

pub fn run_a() {
    println!("Day 3a. Closest intersection is {} units away",
             find_closest_from_input(INPUT))

}

pub fn run_b() {
    println!("Day 3b. Shortest circuit is a combined {} steps",
             find_shortest_circuit_from_input(INPUT))
}

#[test]
fn test_find_closest_from_input() {
    assert_eq!(find_closest_from_input("R8,U5,L5,D3\nU7,R6,D4,L4"), 6);
    assert_eq!(find_closest_from_input(
        "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83"), 159);
    assert_eq!(find_closest_from_input(
        "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7"), 135);
}

#[test]
fn test_find_shortest_circuit_from_input() {
    assert_eq!(find_shortest_circuit_from_input("R8,U5,L5,D3\nU7,R6,D4,L4"), 30);
    assert_eq!(find_shortest_circuit_from_input(
        "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83"), 610);
    assert_eq!(find_shortest_circuit_from_input(
        "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7"), 410);
}